import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ProductService} from '../../services/product.service';
import {ProductModel} from '../../model/product.model';


@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  // @ts-ignore
  products: ProductModel[] = this.getAllProducts();
  editForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private productService: ProductService) {
  }

  getAllProducts(): void {
    this.productService.getAllProducts$().subscribe(data => {
      this.products = data;
    });
  }

  // @ts-ignore
  ngOnInit(): void {
    const productId = localStorage.getItem('productId');
    if (!productId) {
      alert('Something wrong!');
      this.router.navigate(['']);
      return;
    }
    this.editForm = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      quality: ['', Validators.required]
    });

    this.productService.getProductById$(productId).subscribe(data => {
      console.log(data);
      this.editForm.patchValue(data); // Don't use editForm.setValue() as it will throw console error
    });
  }

  // get the form short name to access the form fields
  // tslint:disable-next-line:typedef
  get f() {
    return this.editForm.controls;
  }

  onSubmit(product): void {
    this.submitted = true;
    // @ts-ignore
    this.productService.updateProduct$(this.editForm.value, product.uid)
      .subscribe(data => {
        console.log(data);
        this.router.navigate(['']);
      });
  }


}
