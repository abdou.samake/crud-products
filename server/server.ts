import admin from "firebase-admin";
import {cert} from './cred/cert';

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://mini-projet-b1aee.firebaseio.com"
});
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import {
    deleteProductById,
    getAllProducts,
    getProductlById,
    postNewProduct,
    putProduct,
    updateProduct
} from "./product.service";
import {ProductModel} from "./product.model";

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(cors());


app.get('/api/products', async (req, res) => {
    try {
        const products = await getAllProducts();
        return res.send(products);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});


app.get('/api/products/:id', async (req, res) => {
    try {
        const productId: string = req.params.id;
        const productToFind: ProductModel = await getProductlById(productId);
        return res.send(productToFind);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.delete('/api/products/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const writeResult: string = await deleteProductById(id);
        return res.send(writeResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.post('/api/products', async (req, res) => {
    try {
        const newProduct = req.body;
        const addResult = await postNewProduct(newProduct);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.put('/api/products/:id', async (req, res) => {
    try {
        const id: string = req.params.id;
        const product: ProductModel = await putProduct(id, req.body);
        return res.send(product);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.patch('/api/products/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const product = await updateProduct(id, req.body);
        return res.send(product);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});




app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});


