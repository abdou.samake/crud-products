export interface ProductModel {
    name: string;
    quality: string;
    uid: string;
    price: number;
}
