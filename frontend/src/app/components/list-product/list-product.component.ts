import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductModel} from '../../model/product.model';
import {ProductService} from '../../services/product.service';
import {Router} from '@angular/router';
import {Subject} from 'rxjs';


@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss']
})
export class ListProductComponent implements OnInit {
  products: ProductModel[];
  constructor(private productService: ProductService, private router: Router) { }
  ngOnInit(): void {
    this.getAllProducts();
  }
  getAllProducts(): void {
    this.productService.getAllProducts$().subscribe(data => {
      this.products = data;
    });
  }
  addProduct(): void {
    this.router.navigate(['add-product']);
  }

  deleteProduct(product: ProductModel): void{
    this.productService.deleteProduct$(product.uid)
      .subscribe(data => {
      console.log(data);
      this.getAllProducts();
    });
  }
  // tslint:disable-next-line:typedef
  updateProduct(product: ProductModel){
    localStorage.removeItem('productId');
    localStorage.setItem('productId', product.uid);
    this.router.navigate(['edit-product']);
  }
}
