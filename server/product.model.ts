export interface ProductModel {
    name: string;
    quality: string;
    id: string;
    price: number;
}