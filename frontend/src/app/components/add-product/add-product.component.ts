import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ProductService} from '../../services/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private router: Router, private productService: ProductService) {
  }

  addForm: FormGroup;
  submitted = false;

  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      quality: ['', Validators.required],
    });
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.addForm.valid) {
      this.productService.addProduct$(this.addForm.value)
        .subscribe(data => {
          console.log(data);
          this.router.navigate(['']);
        });
    }
  }
  // tslint:disable-next-line:typedef
  get f() { return this.addForm.controls; }
}
