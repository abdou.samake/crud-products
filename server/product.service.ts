
import {ProductModel} from "./product.model";
import QuerySnapshot = admin.firestore.QuerySnapshot;
import DocumentSnapshot = admin.firestore.DocumentSnapshot;
import DocumentReference = admin.firestore.DocumentReference;
import DocumentData = admin.firestore.DocumentData;
import admin from "firebase-admin";

const db = admin.firestore();

const refProducts = db.collection('products');

export async function getAllProducts(): Promise<ProductModel[]> {
    const productsQuerySnap: QuerySnapshot = await refProducts.get();
    const products: ProductModel[] = [];
    productsQuerySnap.forEach(productSnap => products.push(productSnap.data() as ProductModel));
    return products;
}

export async function getProductlById(productlId: string): Promise<ProductModel> {
    if (!productlId) {
        throw new Error(`productId required`);
    }
    const productToFindRef = refProducts.doc(productlId);
    const ProductToFindSnap: DocumentSnapshot = await productToFindRef.get();
    if (ProductToFindSnap.exists) {
        return ProductToFindSnap.data() as ProductModel;
    } else {
        throw new Error('object does not exists');
    }
}


async function testIfProductExistsById(productId: string): Promise<DocumentReference> {
    const productRef: DocumentReference = refProducts.doc(productId);
    const snapProductToFind: DocumentSnapshot = await productRef.get();
    const productToFind: ProductModel | undefined = snapProductToFind.data() as ProductModel | undefined;
    if (!productToFind) {
        throw new Error(`${productId} does not exists`);
    }
    return productRef;
}

export async function deleteProductById(productId: string): Promise<any> {
    if (!productId) {
        throw new Error('productId is missing');
    }
    const productToDeleteRef: DocumentReference = await testIfProductExistsById(productId);
    await productToDeleteRef.delete();
    return {response : `${productId} -> delete ok` };
}

export async function postNewProduct(newProduct: ProductModel): Promise<ProductModel> {
    if (!newProduct) {
        throw new Error(`new product must be filled`);
    }
    const addResult: DocumentReference<DocumentData> = await refProducts.add(newProduct);
    const createNewProduct: DocumentReference = refProducts
        .doc(addResult.id);
    await createNewProduct.set({...newProduct, ref: createNewProduct, uid: createNewProduct.id});

    return {...newProduct, id: createNewProduct.id};
}

export async function updateProduct(productId: string, newProduct: ProductModel): Promise<ProductModel> {
    if (!newProduct || !productId) {
        throw new Error(`hostel data and product id must be filled`);
    }

    const productToPutRef: DocumentReference = await testIfProductExistsById(productId);
    await productToPutRef.update(newProduct);
    return newProduct;
}

export async function putProduct(productId: string, newProduct: ProductModel): Promise<ProductModel> {
    if (!newProduct || !productId) {
        throw new Error(`product data and product id must be filled`);
    }
    const productToPutRef: DocumentReference = await testIfProductExistsById(productId);
    await productToPutRef.set(newProduct);
    return newProduct;
}
