import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListProductComponent} from './components/list-product/list-product.component';
import {EditProductComponent} from './components/edit-product/edit-product.component';
import {AddProductComponent} from './components/add-product/add-product.component';

const routes: Routes = [
  { path: 'list-product', component: ListProductComponent },
  { path: 'edit-product', component: EditProductComponent },
  { path: 'add-product', component: AddProductComponent},
  { path: '',
    redirectTo: '/list-product',
    pathMatch: 'full'
  },
  ]
;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
