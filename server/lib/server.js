"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const cert_1 = require("./cred/cert");
firebase_admin_1.default.initializeApp({
    credential: firebase_admin_1.default.credential.cert(cert_1.cert),
    databaseURL: "https://mini-projet-b1aee.firebaseio.com"
});
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const product_service_1 = require("./product.service");
const app = express_1.default();
app.use(cors_1.default());
app.use(body_parser_1.default.json());
app.use(cors_1.default());
app.get('/api/products', async (req, res) => {
    try {
        const products = await product_service_1.getAllProducts();
        return res.send(products);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.get('/api/products/:id', async (req, res) => {
    try {
        const productId = req.params.id;
        const productToFind = await product_service_1.getProductlById(productId);
        return res.send(productToFind);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.delete('/api/products/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const writeResult = await product_service_1.deleteProductById(id);
        return res.send(writeResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.post('/api/products', async (req, res) => {
    try {
        const newProduct = req.body;
        const addResult = await product_service_1.postNewProduct(newProduct);
        return res.send(addResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.put('/api/products/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const product = await product_service_1.putProduct(id, req.body);
        return res.send(product);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.patch('/api/products/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const product = await product_service_1.updateProduct(id, req.body);
        return res.send(product);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
//# sourceMappingURL=server.js.map