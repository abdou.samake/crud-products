// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlDataBase: 'http://localhost:3015/api/',
  firebase : {
    apiKey: 'AIzaSyDqkv4oRapO4Y6sUoime8nkjAa2rg9umqc',
    authDomain: 'mini-projet-b1aee.firebaseapp.com',
    databaseURL: 'https://mini-projet-b1aee.firebaseio.com',
    projectId: 'mini-projet-b1aee',
    storageBucket: 'mini-projet-b1aee.appspot.com',
    messagingSenderId: '721806159699',
    appId: '1:721806159699:web:c6288211b825719c14bba6'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
