import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProductModel} from '../model/product.model';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {switchMap, tap} from 'rxjs/operators';
import {AngularFirestoreDocument} from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  product: ProductModel;
  constructor(private http: HttpClient) { }

  apiRest: string = environment.urlDataBase;

  getAllProducts$(): Observable<ProductModel[]>{
    return (this.http.get(this.apiRest + 'products') as Observable<ProductModel[]>);
  }

  getProductById$(id: string): Observable<ProductModel>{
    return this.http.get(this.apiRest + 'products' + '/' + id) as Observable<ProductModel>;
  }

  addProduct$(product: ProductModel): Observable<any>{
    return this.http.post(this.apiRest + 'products', product);
  }

  deleteProduct$(uid: string): Observable<any>{
    return (this.http.delete(this.apiRest + 'products' + '/' + uid) as Observable<any>)
      .pipe(
        tap(product => this.product = product)
      );
  }

  updateProduct$(product: ProductModel, uid: string): Observable<any>{
    return (this.http.patch(this.apiRest + 'products' + '/' + uid, product)as Observable<any>);
  }
}
