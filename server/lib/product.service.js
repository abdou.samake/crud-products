"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const db = firebase_admin_1.default.firestore();
const refProducts = db.collection('products');
async function getAllProducts() {
    const productsQuerySnap = await refProducts.get();
    const products = [];
    productsQuerySnap.forEach(productSnap => products.push(productSnap.data()));
    return products;
}
exports.getAllProducts = getAllProducts;
async function getProductlById(productlId) {
    if (!productlId) {
        throw new Error(`productId required`);
    }
    const productToFindRef = refProducts.doc(productlId);
    const ProductToFindSnap = await productToFindRef.get();
    if (ProductToFindSnap.exists) {
        return ProductToFindSnap.data();
    }
    else {
        throw new Error('object does not exists');
    }
}
exports.getProductlById = getProductlById;
async function testIfProductExistsById(productId) {
    const productRef = refProducts.doc(productId);
    const snapProductToFind = await productRef.get();
    const productToFind = snapProductToFind.data();
    if (!productToFind) {
        throw new Error(`${productId} does not exists`);
    }
    return productRef;
}
async function deleteProductById(productId) {
    if (!productId) {
        throw new Error('productId is missing');
    }
    const productToDeleteRef = await testIfProductExistsById(productId);
    await productToDeleteRef.delete();
    return { response: `${productId} -> delete ok` };
}
exports.deleteProductById = deleteProductById;
async function postNewProduct(newProduct) {
    if (!newProduct) {
        throw new Error(`new product must be filled`);
    }
    const addResult = await refProducts.add(newProduct);
    const createNewProduct = refProducts
        .doc(addResult.id);
    await createNewProduct.set(Object.assign(Object.assign({}, newProduct), { ref: createNewProduct, uid: createNewProduct.id }));
    return Object.assign(Object.assign({}, newProduct), { id: createNewProduct.id });
}
exports.postNewProduct = postNewProduct;
async function updateProduct(productId, newProduct) {
    if (!newProduct || !productId) {
        throw new Error(`hostel data and product id must be filled`);
    }
    const productToPutRef = await testIfProductExistsById(productId);
    await productToPutRef.update(newProduct);
    return newProduct;
}
exports.updateProduct = updateProduct;
async function putProduct(productId, newProduct) {
    if (!newProduct || !productId) {
        throw new Error(`product data and product id must be filled`);
    }
    const productToPutRef = await testIfProductExistsById(productId);
    await productToPutRef.set(newProduct);
    return newProduct;
}
exports.putProduct = putProduct;
//# sourceMappingURL=product.service.js.map